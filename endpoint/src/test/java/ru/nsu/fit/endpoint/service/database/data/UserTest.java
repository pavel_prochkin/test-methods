package ru.nsu.fit.endpoint.service.database.data;

import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import ru.nsu.fit.endpoint.service.exceptions.UserCreationException;

/**
 * Created by username on 09.10.2016.
 */
public class UserTest
{
    @Rule
    public ExpectedException expectedEx = ExpectedException.none();

    @Test
    public void FirstNameNullTest() throws UserCreationException
    {
        expectedEx.expect(UserCreationException.class);
        expectedEx.expectMessage("First name is null");
        new User(null, "Jumbo", "ajaja@ojojo.com", "gonnakillu!");
    }

    @Test
    public void FirstNameLenTwoTest() throws UserCreationException
    {
        User user = new User("Ju", "Jumbo", "ajaja@ojojo.com", "gonnakillu!");
        Assert.assertNotNull(user);
    }

    @Test
    public void FirstNameLenTwelveTest() throws UserCreationException
    {
        User user = new User("Juridicirion", "Jumbo", "ajaja@ojojo.com", "gonnakillu!");
        Assert.assertNotNull(user);
    }

    @Test
    public void FirstNameLenOneTest() throws UserCreationException
    {
        expectedEx.expect(UserCreationException.class);
        expectedEx.expectMessage("First name is too short (minimum length is 2 symbols)");
        new User("J", "Jumbo", "ajaja@ojojo.com", "gonnakillu!");
    }

    @Test
    public void FirstNameLenThirteenTest() throws UserCreationException
    {
        expectedEx.expect(UserCreationException.class);
        expectedEx.expectMessage("First name is too long (maximum length is 12 symbols)");
        new User("Jiriririririm", "Jumbo", "ajaja@ojojo.com", "gonnakillu!");
    }

    @Test
    public void FirstNameNoCapsTest() throws UserCreationException
    {
        expectedEx.expect(UserCreationException.class);
        expectedEx.expectMessage("Invalid First name: must start with a capital letter, no numbers or special symobls allowed");
        new User("jiririri", "Jumbo", "ajaja@ojojo.com", "gonnakillu!");
    }

    @Test
    public void FirstNameTooMuchCapsTest() throws UserCreationException
    {
        expectedEx.expect(UserCreationException.class);
        expectedEx.expectMessage("Invalid First name: must start with a capital letter, no numbers or special symobls allowed");
        new User("JiriRiri", "Jumbo", "ajaja@ojojo.com", "gonnakillu!");
    }

    @Test
    public void FirstNameNumberTest() throws UserCreationException
    {
        expectedEx.expect(UserCreationException.class);
        expectedEx.expectMessage("Invalid First name: must start with a capital letter, no numbers or special symobls allowed");
        new User("Jir1riri", "Jumbo", "ajaja@ojojo.com", "gonnakillu!");
    }

    @Test
    public void FirstNameSpecSymTest() throws UserCreationException
    {
        expectedEx.expect(UserCreationException.class);
        expectedEx.expectMessage("Invalid First name: must start with a capital letter, no numbers or special symobls allowed");
        new User("Jiririri!", "Jumbo", "ajaja@ojojo.com", "gonnakillu!");
    }

    @Test
    public void LastNameNullTest() throws UserCreationException
    {
        expectedEx.expect(UserCreationException.class);
        expectedEx.expectMessage("Last name is null");
        new User("Jumbo", null, "ajaja@ojojo.com", "gonnakillu!");
    }

    @Test
    public void LastNameLenTwoTest() throws UserCreationException
    {
        User user = new User("Jumbo", "Ju", "ajaja@ojojo.com", "gonnakillu!");
        Assert.assertNotNull(user);
    }

    @Test
    public void LastNameLenTwelveTest() throws UserCreationException
    {
        User user = new User("Jumbo", "Juridicirion", "ajaja@ojojo.com", "gonnakillu!");
        Assert.assertNotNull(user);
    }

    @Test
    public void LastNameLenOneTest() throws UserCreationException
    {
        expectedEx.expect(UserCreationException.class);
        expectedEx.expectMessage("Last name is too short (minimum length is 2 symbols)");
        new User("Jumbo", "J", "ajaja@ojojo.com", "gonnakillu!");
    }

    @Test
    public void LastNameLenThirteenTest() throws UserCreationException
    {
        expectedEx.expect(UserCreationException.class);
        expectedEx.expectMessage("Last name is too long (maximum length is 12 symbols)");
        new User("Jumbo", "Jiriririririm", "ajaja@ojojo.com", "gonnakillu!");
    }

}
