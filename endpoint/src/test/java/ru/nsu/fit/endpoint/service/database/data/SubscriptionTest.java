package ru.nsu.fit.endpoint.service.database.data;

import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import ru.nsu.fit.endpoint.service.exceptions.SubscriptionCreationException;

/**
 * Created by username on 09.10.2016.
 */
public class SubscriptionTest
{
    @Rule
    public ExpectedException expectedEx = ExpectedException.none();

    @Test
    public void maxSeatsOneTest() throws SubscriptionCreationException
    {
        Subscription subscription = new Subscription(1, 1, 1);
        Assert.assertNotNull(subscription);
    }

    @Test
    public void maxSeatsNinesTest() throws SubscriptionCreationException
    {
        Subscription subscription = new Subscription(999999, 10, 12);
        Assert.assertNotNull(subscription);
    }

    @Test
    public void maxSeatsZeroTest() throws SubscriptionCreationException
    {
        expectedEx.expect(SubscriptionCreationException.class);
        expectedEx.expectMessage("Max seats value is too small (lower limit is 1)");
        Subscription subscription = new Subscription(0, 1, 1);
    }

    @Test
    public void maxSeatsThousandsTest() throws SubscriptionCreationException
    {
        expectedEx.expect(SubscriptionCreationException.class);
        expectedEx.expectMessage("Max seats value is too big (upper limit is 999999)");
        Subscription subscription = new Subscription(1000000, 10, 12);
    }

    @Test
    public void minSeatsOneTest() throws SubscriptionCreationException
    {
        Subscription subscription = new Subscription(10, 1, 3);
        Assert.assertNotNull(subscription);
    }

    @Test
    public void minSeatsNinesTest() throws SubscriptionCreationException
    {
        Subscription subscription = new Subscription(999999, 999999, 999999);
        Assert.assertNotNull(subscription);
    }

    @Test
    public void minSeatsZeroTest() throws SubscriptionCreationException
    {
        expectedEx.expect(SubscriptionCreationException.class);
        expectedEx.expectMessage("Min seats value is too small (lower limit is 1)");
        Subscription subscription = new Subscription(10, 0, 1);
    }

    @Test
    public void minSeatsThousandsTest() throws SubscriptionCreationException
    {
        expectedEx.expect(SubscriptionCreationException.class);
        expectedEx.expectMessage("Min seats value is too big (upper limit is 999999)");
        Subscription subscription = new Subscription(999999, 1000000, 999999);
    }

    @Test
    public void minSeatsTooBigTest() throws SubscriptionCreationException
    {
        expectedEx.expect(SubscriptionCreationException.class);
        expectedEx.expectMessage("Min seats value cannot be greater than max seats value");
        Subscription subscription = new Subscription(78, 100, 50);
    }

    @Test
    public void usedSeatsTooBigTest() throws SubscriptionCreationException
    {
        expectedEx.expect(SubscriptionCreationException.class);
        expectedEx.expectMessage("Used seats value cannot be greater than max seats value");
        Subscription subscription = new Subscription(78, 50, 100);
    }

    @Test
    public void usedSeatsTooSmallTest() throws SubscriptionCreationException
    {
        expectedEx.expect(SubscriptionCreationException.class);
        expectedEx.expectMessage("Used seats value must be greater than min seats value");
        Subscription subscription = new Subscription(78, 50, 10);
    }

    @Test
    public void usedSeatsGoodTest() throws SubscriptionCreationException
    {
        Subscription subscription = new Subscription(1000, 500, 750);
        Assert.assertNotNull(subscription);
    }
}
