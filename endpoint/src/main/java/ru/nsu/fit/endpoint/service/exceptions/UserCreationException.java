package ru.nsu.fit.endpoint.service.exceptions;

/**
 * Created by паша on 09.10.2016.
 */
public class UserCreationException extends ObjectCreationException
{
    public UserCreationException(String msg)
    {
        super(msg);
    }
}
