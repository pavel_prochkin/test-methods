package ru.nsu.fit.endpoint.service.exceptions;

import javax.jws.soap.SOAPBinding;

/**
 * Created by паша on 09.10.2016.
 */
public class ObjectCreationException extends Exception
{
    public ObjectCreationException()
    {
        super("");
    }

    public ObjectCreationException(String msg)
    {
        super(msg);
    }
}
