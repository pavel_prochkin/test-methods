package ru.nsu.fit.endpoint.service.database.data;

import ru.nsu.fit.endpoint.service.exceptions.UserCreationException;

import java.util.UUID;
import java.util.regex.Pattern;
/**
 * @author Timur Zolotuhin (tzolotuhin@gmail.com)
 */
public class User {
    private UUID customerId;
    private UUID[] subscriptionIds;
    private UUID id;
    /* нет пробелов, длина от 2 до 12 символов включительно, начинается с заглавной буквы, остальные символы строчные, нет цифр и других символов */
    private String firstName;
    /* нет пробелов, длина от 2 до 12 символов включительно, начинается с заглавной буквы, остальные символы строчные, нет цифр и других символов */
    private String lastName;
    /* указывается в виде email, проверить email на корректность */
    private String login;
    /* длина от 6 до 12 символов включительно, недолжен быть простым, не должен содержать части login, firstName, lastName */
    private String pass;
    private UserRole userRole;

    private static final String nameFormat = "^[A-Z][a-z]+";
    private static final String loginFormat = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@" + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
    private static final String easyPassword = "qwe123";
    private static final int nameMaxLen = 12;
    private static final int nameMinLen = 2;
    private static final int passwordMaxLen = 12;
    private static final int passwordMinLen = 6;

    public User(String firstName, String lastName, String login, String pass) throws UserCreationException
    {
        checkName(firstName, "First");
        this.firstName = firstName;

        checkName(lastName, "Last");
        this.lastName = lastName;

        checkLogin(login);
        this.login = login;

        checkPassword(pass);
        this.pass = pass;
    }

    public static enum UserRole
    {
        COMPANY_ADMINISTRATOR("Company administrator"),
        TECHNICAL_ADMINISTRATOR("Technical administrator"),
        BILLING_ADMINISTRATOR("Billing administrator"),
        USER("User");

        private String roleName;

        UserRole(String roleName) {
            this.roleName = roleName;
        }

        public String getRoleName() {
            return roleName;
        }
    }

    private void checkName(String name, String whichOne) throws UserCreationException
    {
        if(name == null)
            throw new UserCreationException(whichOne + " name is null");

        if(name.length() < nameMinLen)
            throw new UserCreationException(whichOne + " name is too short (minimum length is 2 symbols)");

        if(name.length() > nameMaxLen)
            throw new UserCreationException(whichOne + " name is too long (maximum length is 12 symbols)");

        if(!name.matches(nameFormat))
            throw new UserCreationException("Invalid " + whichOne + " name: must start with a capital letter, no numbers or special symobls allowed");
    }

    private void checkLogin(String login) throws UserCreationException
    {
        if(login == null)
            throw new UserCreationException("Login is null");

        if(!login.matches(loginFormat))
            throw new UserCreationException("Invalid login: must be a valid email address");
    }

    private void checkPassword(String password) throws UserCreationException
    {
        if(password == null)
            throw new UserCreationException("Password is null");

        if(password.length() < passwordMinLen)
            throw new UserCreationException("Password is too short (minimum length is 6 symbols)");

        if(password.length() > passwordMaxLen)
            throw new UserCreationException("Password is too long (maximum length is 12 symbols)");

        if(password.toLowerCase().contains(firstName.toLowerCase()))
            throw new UserCreationException("Password contains first name");

        if(password.toLowerCase().contains(lastName.toLowerCase()))
            throw new UserCreationException("Password contains last name");

        if(password.toLowerCase().contains(login.toLowerCase()))
            throw new UserCreationException("Password contains login");

        if(password.equals(easyPassword))
            throw new UserCreationException("Password is too easy");
    }
}
