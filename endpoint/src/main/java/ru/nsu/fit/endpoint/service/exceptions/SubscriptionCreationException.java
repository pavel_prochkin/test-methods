package ru.nsu.fit.endpoint.service.exceptions;

/**
 * Created by паша on 09.10.2016.
 */
public class SubscriptionCreationException extends ObjectCreationException
{
    public SubscriptionCreationException(String msg)
    {
        super(msg);
    }
}
