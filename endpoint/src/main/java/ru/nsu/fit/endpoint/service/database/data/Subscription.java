package ru.nsu.fit.endpoint.service.database.data;

import ru.nsu.fit.endpoint.service.exceptions.SubscriptionCreationException;

import java.util.UUID;

/**
 * @author Timur Zolotuhin (tzolotuhin@gmail.com)
 */
public class Subscription {
    private UUID id;
    private UUID customerId;
    private UUID servicePlanId;
    private int maxSeats;
    private int minSeats;
    private int usedSeats;

    private static final int seatsLowerLimit = 1;
    private static final int seatsUpperLimit = 999999;

    public Subscription(int maxSeats, int minSeats, int usedSeats) throws SubscriptionCreationException
    {
        if(maxSeats < seatsLowerLimit)
            throw new SubscriptionCreationException("Max seats value is too small (lower limit is 1)");

        if(minSeats < seatsLowerLimit)
            throw new SubscriptionCreationException("Min seats value is too small (lower limit is 1)");

        if(maxSeats > seatsUpperLimit)
            throw new SubscriptionCreationException("Max seats value is too big (upper limit is 999999)");

        if(minSeats > seatsUpperLimit)
            throw new SubscriptionCreationException("Min seats value is too big (upper limit is 999999)");

        if(minSeats > maxSeats)
            throw new SubscriptionCreationException("Min seats value cannot be greater than max seats value");

        if(usedSeats > maxSeats)
            throw new SubscriptionCreationException("Used seats value cannot be greater than max seats value");

        if(usedSeats < minSeats)
            throw new SubscriptionCreationException("Used seats value must be greater than min seats value");

        this.maxSeats = maxSeats;
        this.minSeats = minSeats;
        this.usedSeats = usedSeats;
    }
}
